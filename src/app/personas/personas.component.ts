import { Component } from '@angular/core';


@Component({
    selector: 'app-personas',
    templateUrl: './personas.component.html',
    styleUrls: ['./personas.component.css']

})
export class PersonasComponent{
    agregarPersona= false;
    agregarPersonasStatus= "No se ha agregado personas";
    tituloPersona= "";
    personaCreada= false;

    constructor(){
        setTimeout(
            () => {
                this.agregarPersona = true;
            }
            ,3000)
    }

    onCrearPersona(){
        this.personaCreada = true;
        this.agregarPersonasStatus = "Persona agregada";
    }

    onModificarPersona(event: Event){
        this.tituloPersona = (<HTMLInputElement>event.target).value;
    }
}