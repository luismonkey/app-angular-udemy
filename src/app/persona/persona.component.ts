import { Component } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent  {

  nombrePersona:string = "Luis";
  apellidoPersona:string = "Gomez";
  edad:number = 25;

  constructor() { }
}